variable "subnet_id" {
  type = string
  
}

variable "region" {
  type = string
  
}

variable "instance_type" {
  type = string
  
}

variable "instance_tags" {
  type = map
    
    default = {
       name = "helloworld"
       user = "sandeep"
       purpose = "dev"


    }
}
#variable "ssh_key" {
 # type = string
  
#}

variable "timezone" {
  type = string
  
}

variable "hostname" {
  type = string
  
}

variable "new_user" {
  type = string
  
}
