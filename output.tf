# Outputs file
output "ec2_pub_url" {
  value = aws_eip.foo.public_dns
}

output "ec2_pub_ip" {
  value = aws_eip.foo.public_ip
}
